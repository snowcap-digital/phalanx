var response = require('../../common/response');
var db = require('../../common/db')
var { generatePassword } = require('../../common/crypto-utils')
var { sign } = require('../../common/jwt')

exports.debug = async function (req, res) {
    try {
        const users = await db(req.params.table).select('*');

        return response.success(res, users);
    }
    catch (err) {
        return response.systemError(res, err);
    }
};
exports.getAll = async function (req, res) {
    try {
        const users = await db('users').select('*');

        return response.success(res, users);
    }
    catch (err) {
        return response.systemError(res, err);
    }
};

exports.get = async function (req, res) {
    try {
        const users = await db('users')
            .where({ id: req.params.id })
            .first('*')

        return response.success(res, users);
    }
    catch (err) {
        return response.systemError(res, err);
    }
};

exports.create = async function (req, res) {

    try {
        const userExists = await db('users')
            .where({ username: req.params.username })
            .orWhere({ email: req.body.email })
            .select('*');

        if (userExists.length > 0) {
            return response.error(res, 'User already exists', 403);
        }

        db.transaction(function (trx) {
            return trx.insert({ username: req.params.username, email: req.body.email }, '*')
                .into('users')
                .then(async (insertedUsers) => {
                    //inserted user, now add their password
                    const insertedUser = insertedUsers[0];
                    const userId = insertedUser.id;

                    const { hashedPassword, salt } = generatePassword(req.body.password);
                    return trx.insert({ user_id: userId, password: hashedPassword, salt }).into('logins').then(() => insertedUser);
                })
        })
            .then(function (user) {
                const jwt = sign(user);
                return response.success(res, jwt);
            })
            .catch((err) => {
                return response.systemError(res, err);
            });
    }
    catch (err) {
        return response.systemError(res, err);
    }
};
