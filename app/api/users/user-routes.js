var express = require('express');
const validators = require('./user-validators');
const { validate } = require('express-validation')

var users = require('./users')
var router = express.Router();

router.get('/', users.getAll);

router.get('/debug/:table', users.debug)
router.get('/user/:id', validate(validators.get), users.get);

router.post('/username/:username', validate(validators.create, {}, { abortEarly: false }), users.create);

module.exports = router;
