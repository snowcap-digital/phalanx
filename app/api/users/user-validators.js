const { Joi } = require('express-validation');

const validators = {
    get: {
        params: Joi.object({
            id: Joi.number().required()
        })
    },
    create: {
        params: Joi.object({
            username: Joi.string().min(3).required()
        }),
        body: Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().min(8).required()
        })
    }
}
module.exports = validators;