var express = require('express');
var users = require('./users/user-routes')
var auth = require('./auth/auth-routes')
var router = express.Router();

router.use('/auth', auth);
router.use('/users', users);

module.exports = router;
