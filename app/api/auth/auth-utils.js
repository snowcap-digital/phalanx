var db = require('../../common/db')
var { hashPassword } = require('../../common/crypto-utils');

const validateCredentials = async function(username, password){
    const stored = await db('logins').select('*')
                .join('users', `users.id`, `logins.user_id`)
                .where({username})
                .first();
    
    let success = false;
    const hashDetails = hashPassword(password, stored.salt)
    if (stored.password === hashDetails.hashedPassword) {
        success = true;    
    }

    return success;
}

module.exports = {
    validateCredentials
}