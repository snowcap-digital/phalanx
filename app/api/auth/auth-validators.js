const { Joi } = require('express-validation');

const validators = {
    login: {
        body: Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required()
        })
    }
}
module.exports = validators;