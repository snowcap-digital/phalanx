var express = require('express');
const validators = require('./auth-validators');
const { validate } = require('express-validation')

var auth = require('./auth')
var router = express.Router();



router.get('/verify', auth.verify); // No Pre validation, they either have a token or dont 
router.post('/login', validate(validators.login), auth.login);


module.exports = router;
