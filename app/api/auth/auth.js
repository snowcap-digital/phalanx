var response = require('../../common/response');
var db = require('../../common/db')
var { generatePassword } = require('../../common/crypto-utils')
var { validateCredentials } = require('./auth-utils');
var { sign, verify } = require('../../common/jwt')

exports.login = async function (req, res) {
    // Validate credentials, 
    try {
        const { username, password } = req.body;
        const validateSuccess = await validateCredentials(username, password);

        if (validateSuccess) {
            const user = await db('users').select('*')
                .where({ username })
                .first();

            const jwt = sign(user);
            return response.success(res, jwt)
        }
        throw new Error('Invalid Credentials');
    }
    catch (err) {
        return response.error(res, 'Invalid credentials', 401);
    }
};
exports.verify = function (req, res) {
    try {
        const decoded = verify(req.token);
        return response.success(res, decoded)
    }
    catch (err) {
        return response.error(res, 'Invalid token', 401);
    }
};