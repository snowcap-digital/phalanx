
var config = { env: process.env.NODE_ENV };

switch (config.env) {
	case 'local':
	case 'dev':
		config.allowedOrigins = [
			'http://localhost:3000', 
			'http://food.nexus:3000',
			'https://localhost:3000', 
			'https://food.nexus:3000',
            'https://local.foodnex.us:1234'
		]
		config.port = 443
		config.jwtSecret = 'idkmybffjill';
		config.jwtTTL = 69420
		config.jwtAudience = 'FoodNexus';
		config.jwtIssuer = 'Fnexus-Phalanx';
		break;
	case 'test':
	case 'prod':
	default:
		config.port = 443
		break;
}


module.exports = config;