var express		= require( 'express' );
var app			= express();
var bodyParser	= require( 'body-parser' );
var morgan		= require( 'morgan' );
var mongoose	= require( 'mongoose' );
var jwt			= require( 'jsonwebtoken' );
var bearerToken = require('express-bearer-token');
var util = require('util')
var{ ValidationError } = require('express-validation')
var config		= require( './config' );
var routes	= require( './api/routes' );
var cors = require('cors');
var https = require('https');
var fs = require('fs');

const corsOptions = (req, callback) => {
	const opts = {origin: false, credentials: true};
	callback(null, true)
}
app.use(cors(corsOptions))
//Connect middleware
app.use( bodyParser.urlencoded({ extended: false }));
app.use( bodyParser.json() );
app.use( morgan( 'dev' ) );
app.use( bearerToken())

app.use('/', (req,res,next) => {
	console.log(req.headers)
	next();
})
//Routes
app.use('/', routes);

app.get( '/', function( request, response ){
	response.send( 'Phalanx is aware of you.' );
});

//Error handling
app.use( (error, req, res, next) => {
	//joi/express-validation
	if (error instanceof ValidationError) {
		return res.status(error.statusCode).json(error)
	}


	if( error instanceof SyntaxError && error.status == 400 && 'body' in error){
		//failed to parse POSTbody
		return res.status(400).json({errors: [{location:'body','messages': ['Invalid JSON']}]})
	}

	res.error = error;
	res.errorMessage = error.message;
	res.errorStack = error.stack;

	//Other uncaught errors, it might be a runtime error too
	if( process.env != 'prod') {
		return res.status(500).send(util.inspect(error));
	}

	res.status.send(500);
})
//If no route matched by now, 404 it.
app.use(( req, res, next) => {
	res.status(404);
	res.json({status:404, message: 'Not Found'})
});

// Start the server
// app.listen( config.port );
var privateKey = fs.readFileSync( '/usr/src/certs/privkey.pem' );
var certificate = fs.readFileSync( '/usr/src/certs/cert.pem' );
https.createServer({
    key: privateKey,
    cert: certificate
}, app).listen(config.port);

console.log( 'Phalanx is running');
