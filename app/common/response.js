exports.success = (res, data = [], metadata = {}, code = 200) => res.status(code).json({ data, metadata });

exports.noContentSuccess = res => res.status(204).json();

exports.notFound = res => res.status(404).json();
// Use this error when intentionally responding with an error code
exports.error = (res, message = 'Server Error', code = 500, metadata) => {
    res.errors = message;

    return res.status(code).json({ errors: [{ message }], metadata })
}

// Use this error when catching unexpected but possible errors
exports.systemError = (res, err, message = 'Server Error', code = 500) => {
    if (err) {
        res.errorMessage = err.message;
        res.errorStack = err.stack;

        console.log(err.message, err.stack);
    }

    return res.status(code).json({ errors: [{ message }] })
}