var jwt = require('jsonwebtoken');
const {jwtSecret, jwtTTL, jwtAudience, jwtIssuer} = require('../config');


function sign( details ){
    return jwt.sign(
        details, 
        jwtSecret, 
        {
            subject: details.user_id,
            issuer: jwtIssuer,
            expiresIn: jwtTTL,
            audience: jwtAudience
        }
    );
}

function verify(token){
    return jwt.verify(token, jwtSecret);
}

module.exports = {
    sign,
    verify
}