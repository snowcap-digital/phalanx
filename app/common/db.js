require('dotenv').config()

const knex = require('knex')
const { camelCase, mapKeys, snakeCase } = require('lodash')

const {
    DBCLIENT,
    PGHOST,
    PGDATABASE,
    PGPASSWORD,
    PGUSER
} = process.env

const db = knex({
    client: DBCLIENT,
    connection: {
        database: PGDATABASE,
        host: PGHOST,
        password: PGPASSWORD,
        user: PGUSER
    },
    // convert response keys from snake case to camel case
    postProcessResponse(response) {
        if (Array.isArray(response)) {
            return response.map((row) => mapKeys(row, (_, key) => camelCase(key)))
        }
        else {
            return mapKeys(response, (_, key) => camelCase(key))
        }
    },
    wrapIdentifier(value, originalImplmentation) {
        if (value === '*') {
            return originalImplmentation(value)
        }
        else {
            return originalImplmentation(snakeCase(value))
        }
    }
})

module.exports = db