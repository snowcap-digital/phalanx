FROM node:18
EXPOSE 80

COPY /app /usr/src/app
COPY /certs /usr/src/certs
COPY /package.json /usr/src/package.json
COPY knexfile.js /usr/src/knexfile.js
RUN cd /usr/src && npm install

WORKDIR "/usr/src/app"

CMD node server.js