// Update with your config settings.

module.exports = {
    client: 'pg',
    connection: {
        host: process.env.PGHOST || 'fnexusdb',
        database: process.env.PGDATABASE || 'fnexus',
        user: process.env.PGUSER || 'fnexus_owner',
        password: process.env.PGPASSWORD || 'devpassword',
        port: process.env.PGPORT || '3003'
    }
};
